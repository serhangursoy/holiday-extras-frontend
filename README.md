# Holiday Extras Flickr Assignment

## Requirements

Make sure that you have nodejs installed on your machine. Also, you need npm or yarn.

## Install Deps

    $ git clone https://gitlab.com/serhangursoy/holiday-extras-frontend.git
    $ cd holiday-extras-client
    $ npm install

## Start

    $ npm start

## Test

    $ npm test


## Helpers

### Framework

- [React](https://reactjs.org/) main framework.

### Style

- [Creative Tim](https://www.creative-tim.com/) is used to make the app look fancy with the help of Material Design that is supported by Google.

### Test

- [jest](https://jestjs.io/) is used to create tests and checking the component integrity and functionality.
