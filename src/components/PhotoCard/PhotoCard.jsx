import React from "react";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';


const styleCard = {maxWidth: 200, padding: 10, margin: 20, minWidth: 200, minHeight: 300, maxHeight: 350};

class PhotoCard extends React.Component {
  state = {};

  constructor(props){
    super();
    if (props) {
      this.state = { data: props.data };
    }
  }

  render() {
    let user = this.state.data.author.match(/(?:"[^"]*"|^[^"]*$)/)[0].replace(/"/g, "");
    return (
      <div>
        <Card style={styleCard}>
          <CardActionArea href={ this.state.data.link } >
           <CardMedia
             style={{height: 140, minWidth: 100}}
             className="cardMedia"
             image={this.state.data.media.m}
           />
           </CardActionArea>
           <CardContent style={{ maxHeight:100, textOverflow:"ellipsis", display:"block",overflow:"hidden"}}>
            <a href={ this.state.data.link } style={{ maxHeight:100, textOverflow:"ellipsis"}}>
              { this.state.data.title }
            </a>   by:
            <a href={ "https://www.flickr.com/people/"+user }>
              { user }
            </a>
           </CardContent>
         <CardActions>
          <div>
            <Typography variant="body2" color="textSecondary" component="p">
             Date: <b> { (new Date(this.state.data.published)).toLocaleDateString("en-GB") }  </b>
             </Typography>
             <Typography variant="caption" display="block" gutterBottom style={{ maxHeight:20, textOverflow:"ellipsis",overflow:"hidden"}}>
                Tags: <b> { this.state.data.tags } </b>
            </Typography>
          </div>
         </CardActions>
       </Card>
      </div>
    );
  }
}

export default PhotoCard;
