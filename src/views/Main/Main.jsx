import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import GridContainer from "components/Grid/GridContainer.jsx";
import PhotoCard from "components/PhotoCard/PhotoCard.jsx";
import mainStyle from "assets/jss/material-main-react/views/mainStyle.jsx";
const URI = "http://api.flickr.com/services/feeds/photos_public.gne?nojsoncallback=true&format=json";

class Main extends React.Component {
  state = {
    didLoad: false,
    items: []
  };

  componentDidMount() {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    fetch(proxyurl + URI)
      .then(response => response.json())
      .then(data => {
        this.setState({ didLoad: true, items: data.items });
      }).catch(function(error) {
        console.log(error);
      });
  }

  render() {

    // If nothing loaded, simply show the loading screen
    if (!this.state.didLoad) {
      return (
        <div className="loading-back" id="loadingPanel">
          <GridContainer>
            <div className="loading-indicator"></div>
            <h2 className="loading-text">Loading</h2>
          </GridContainer>
        </div>
      );
    }

    const listItems = this.state.items.map((item, i) =>
      <PhotoCard key={i} data={item}></PhotoCard>
    );

    return (
      <div>
        <GridContainer align="center">
          <Typography variant="h3" align="center" style={{ marginTop: "20px" }} id="mainTitle">
           Flickr Public Photos
          </Typography>
        </GridContainer>
        <GridContainer>
          { listItems }
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(mainStyle)(Main);
